Clock = 12MHz
JA-1:     Input Signal
JA-2:     Serialized 32 bit frequency value
JA-3:     Clock for serialized output. Sample on falling edge
PIO1/14-17/18: 16 least significant bits of frequency value

