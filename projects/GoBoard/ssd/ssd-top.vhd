library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ssd_top is
generic (CLK_DIVISOR :  POSITIVE := 1000000);
  port (
    clk          : in  std_logic;
    o_Segment1_A : out std_logic;
    o_Segment1_B : out std_logic;
    o_Segment1_C : out std_logic;
    o_Segment1_D : out std_logic;
    o_Segment1_E : out std_logic;
    o_Segment1_F : out std_logic;
    o_Segment1_G : out std_logic;
    o_Segment2_A : out std_logic;
    o_Segment2_B : out std_logic;
    o_Segment2_C : out std_logic;
    o_Segment2_D : out std_logic;
    o_Segment2_E : out std_logic;
    o_Segment2_F : out std_logic;
    o_Segment2_G : out std_logic
    );
end entity ssd_top;

architecture RTL of ssd_top is

-- Clock divider
signal clk_div_cnt : unsigned (31 downto 0) := (others => '0');
signal div_clk     : std_logic := '0';

signal digit   : unsigned (7 downto 0) := (others => '0');
signal s_digit : std_logic_vector (7 downto 0) := (others => '0');

begin

    -- Clock divider
    clk_divider: process(clk)
    begin
	if(rising_edge(clk)) then
		if clk_div_cnt = (CLK_DIVISOR-1) then
			clk_div_cnt <= (others => '0');
			div_clk <= not div_clk;
		else
			clk_div_cnt <= clk_div_cnt + 1;
		end if;
	end if;
    end process clk_divider;

  -- Increment digit
    digit_incr: process(div_clk)
    begin
	if(rising_edge(div_clk)) then
		digit <= digit + 1;
	end if;
    end process digit_incr;


 --Instantiate double SSD
   DIGITS: entity work.ssd_two_digits
    port map(
    i_Clk        => div_clk,
    i_Binary_Num => s_digit,
    o_Segment_1A => o_Segment1_A,
    o_Segment_1B => o_Segment1_B,
    o_Segment_1C => o_Segment1_C,
    o_Segment_1D => o_Segment1_D,
    o_Segment_1E => o_Segment1_E,
    o_Segment_1F => o_Segment1_F,
    o_Segment_1G => o_Segment1_G,
    o_Segment_2A => o_Segment2_A,
    o_Segment_2B => o_Segment2_B,
    o_Segment_2C => o_Segment2_C,
    o_Segment_2D => o_Segment2_D,
    o_Segment_2E => o_Segment2_E,
    o_Segment_2F => o_Segment2_F,
    o_Segment_2G => o_Segment2_G 
);

s_digit <= std_logic_vector(digit);

end RTL;
