library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity top is

 Port ( 
           REDn        : out  STD_LOGIC;
           BLUn        : out  STD_LOGIC;
           GRNn        : out  STD_LOGIC;
           clk_out     : out  STD_LOGIC
);
end top;

architecture Behavioral of top is

-- Testbench signals
signal r_clk      : STD_LOGIC := '0';
signal r_red      : STD_LOGIC := '0';
signal r_green    : STD_LOGIC := '0';
signal r_blue     : STD_LOGIC := '0';

-- Insantiate top components
component blinkyrgb     Port (clk : in STD_LOGIC; red : out STD_LOGIC; green : out STD_LOGIC; blue : out  STD_LOGIC); end component;
component CLOCK_SRC     Port (clock : out STD_LOGIC); end component;
component RGB           Port (red : in STD_LOGIC; green: in STD_LOGIC; blue: in STD_LOGIC; REDn : out STD_LOGIC; BLUn : out STD_LOGIC; GRNn : out STD_LOGIC); end component;

begin

U1 : blinkyrgb   port map (clk => r_clk, red => r_red, green => r_green, blue => r_blue);
U2 : CLOCK_SRC   port map (clock => r_clk);
U3 : RGB         port map (red => r_red, green => r_green, blue => r_blue, REDn => REDn, BLUn => BLUn, GRNn => GRNn);

clk_out <= r_clk;

end Behavioral;
