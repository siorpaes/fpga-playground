Clock = 100MHz
JC-1:     Serialized 32 bit frequency value. Read with provided Saleae configuration
JC-2:     Input Signal
JC-3:     Clock for serialized output. Sample on falling edge
JA/JB:    16 least significant bits of current frequency value

