library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity spi_tb is
end spi_tb;

architecture Behavioral of spi_tb is

-- Clock 100MHZ
constant c_CLOCK_PERIOD : time     := 10 ns;

-- Start signal fires every us
constant c_START_TIME   : time     := 1 us;


-- Testbench signals
signal r_clk        : STD_LOGIC := '0';
signal r_reset      : STD_LOGIC := '0';
signal r_start      : STD_LOGIC := '0';
signal r_sclk       : STD_LOGIC := '0';
signal r_mosi       : STD_LOGIC := '0';
signal r_miso       : STD_LOGIC := '0';
signal r_busy       : STD_LOGIC := '0';
signal r_divisor    : STD_LOGIC_VECTOR (7 downto 0);
signal r_txdata     : STD_LOGIC_VECTOR (7 downto 0);
signal r_rxdata     : STD_LOGIC_VECTOR (7 downto 0);

begin

-- Instantiate UUT. Use less verbose entity instantiation
UUT: entity work.spi
    port map(
    reset    => r_reset,
    clk      => r_clk,
    txdata   => r_txdata,
    rxdata   => r_rxdata,
    busy     => r_busy,
    start    => r_start,
    divisor  => r_divisor,
    sclk     => r_sclk,
    mosi     => r_mosi,
    miso     => r_miso 
);

-- Assign signals
r_reset   <= '0';
r_txdata  <= "10000001";
r_divisor <= x"04";
r_miso    <= '1';

-- Generate Clock
p_CLK_GEN : process is
  begin
    wait for c_CLOCK_PERIOD/2;
    r_clk <= not r_clk;
end process p_CLK_GEN;

-- Generate Start signal
p_START_GEN : process is
  begin
    r_start <= '0';
    wait for c_START_TIME;
    r_start <= '1';
    --Start signal lasts for 8 clock periods
    wait for 8*c_CLOCK_PERIOD;
end process p_START_GEN;
end Behavioral;
