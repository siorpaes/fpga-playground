----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/19/2018 01:05:58 PM
-- Design Name: 
-- Module Name: cdelay_top - Behavioral
-- Description:
-- Top file to test cdelay design. Feeds a data and a clock to cdelay.
-- Observe with scope that the delay introduced is coherent with what
-- is specified on the dly_data() input
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

entity cdelay_top is
    Port ( clk      : in STD_LOGIC;
           delay    : in STD_LOGIC_VECTOR (4 downto 0);
           clkout   : out STD_LOGIC;
           data     : out STD_LOGIC;
           dly_data : out STD_LOGIC);
end cdelay_top;

architecture Behavioral of cdelay_top is

signal r_clk        : STD_LOGIC := '0';
signal r_clkout     : STD_LOGIC := '0';
signal r_data       : STD_LOGIC := '0';
signal r_dly_data   : STD_LOGIC := '0';
signal r_output     : STD_LOGIC := '0';
signal r_delay      : STD_LOGIC_VECTOR (4 downto 0);

begin

CLKGEN: entity work.clkgen
port map(
	clk    => r_clk,
	clkout => r_clkout,
	data   => r_data
);

CDELAY: entity work.cdelay
port map(
	clk    => r_clkout,
	input  => r_data,
	dly    => r_delay,
	output => r_output
);

r_clk    <= clk;
r_delay  <= delay;
data     <= r_data;
dly_data <= r_output;
clkout   <= r_clkout;

end Behavioral;
