----------------------------------------------------------------------------------
-- Company: STMicroelectronics
-- Engineer: David Siorpaes
-- 
-- Create Date: 07/19/2018 11:02:03 AM
-- Module Name: cdelay - Behavioral
-- Description: 
-- Implements a configurable delay line. Use dly vector to specify the delay in clock periods. 
-- Min delay is 1 pulse. Max delay is 32 pulses
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity cdelay is
Port ( 
	clk    : in STD_LOGIC;
	input  : in STD_LOGIC;
	dly    : in STD_LOGIC_VECTOR (4 downto 0);
	output : out STD_LOGIC);
end cdelay;

architecture Behavioral of cdelay is

begin

process(clk, dly)
	variable q : STD_LOGIC_VECTOR (0 to 31) := (OTHERS => '0');
begin
	if rising_edge(clk) then
		q := input & q(0 to 30);
	end if;
	output <= q(to_integer(unsigned(dly)));
end process;

end Behavioral;
