----------------------------------------------------------------------------------
-- Company: STMicroelectronics
-- Engineer: David Siorpaes
-- 
-- Create Date: 07/19/2018 11:19:21 AM

-- Description: 
-- Emits two cascading clocks
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity clkgen is
generic ( CLK_DIVISOR1 :  POSITIVE := 32;
          CLK_DIVISOR2 :  POSITIVE := 16);
             
Port ( clk    : in STD_LOGIC;
       clkout : out STD_LOGIC;
       data   : out STD_LOGIC);
end clkgen;

architecture Behavioral of clkgen is

-- Clock divider
signal clk_div_cnt1      : unsigned (31 downto 0) := (others => '0');
signal clk_div_cnt2      : unsigned (31 downto 0) := (others => '0');
signal clk_div1          : std_logic := '0';
signal clk_div2          : std_logic := '0';

begin
process(clk)                                              
begin                                                     
        if rising_edge(clk) then                         
                if clk_div_cnt1 = (CLK_DIVISOR1-1) then
                    clk_div_cnt1 <= (others => '0');
                    clk_div1 <= not clk_div1;
                else
                    clk_div_cnt1 <= clk_div_cnt1 + 1;
                end if;                
        end if;                                           
end process;

process(clk_div1)
begin
        if rising_edge(clk_div1) then
                if clk_div_cnt2 = (CLK_DIVISOR2-1) then     
                    clk_div_cnt2 <= (others => '0');
                    clk_div2 <= not clk_div2;            
                else
                    clk_div_cnt2 <= clk_div_cnt2 + 1;   
                end if;
		end if;
end process;

clkout <= clk_div1;
data   <= clk_div2;

end Behavioral;
