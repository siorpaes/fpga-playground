module blinky
(
    i_clock,
    o_led
);

input i_clock;
output o_led;

parameter c_CNT = 2500000;
reg[31:0] counter = 0;
reg r_led;

always @ (posedge i_clock)
begin
    if (counter == c_CNT - 1)
        begin
            r_led <= !r_led;
            counter <= 0;
        end
    else
        counter <= counter + 1;
end

assign o_led = r_led;

endmodule
