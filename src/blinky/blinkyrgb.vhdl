library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- CLK_FREQ:     Input clock frequency
entity blinkyrgb is
    Port ( clk        : in  STD_LOGIC;   -- Input clock
           red        : out  STD_LOGIC;
           green      : out  STD_LOGIC;
           blue       : out  STD_LOGIC);
end blinkyrgb;

architecture Behavioral of blinkyrgb is

-- Clock counter
signal clk_cnt     : unsigned (31 downto 0) := (others => '0');

-- LED driver signal
signal led_driver  : unsigned (2 downto 0) := (others => '0');


begin

process(clk)
begin
	if(rising_edge(clk)) then
		clk_cnt <= clk_cnt + 1;
	end if;
end process;

-- Update driver signal
led_driver <= clk_cnt (26 downto 24);

-- Drive LEDs
red   <= led_driver(0);
green <= led_driver(1);
blue  <= led_driver(2);

end Behavioral;
