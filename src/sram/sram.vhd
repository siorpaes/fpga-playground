-- Simple SRAM implementation

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity sram is
port(
	address   : in  std_logic_vector(5 downto 0);
	datain    : in  std_logic_vector(7 downto 0);
	clk       : in  std_logic;
	we        : in  std_logic;
	dataout   : out std_logic_vector(7 downto 0)
);
end sram;

architecture Behavioral of sram is

type memory_array is array(0 to 63) of std_logic_vector(7 downto 0);

signal memory : memory_array := (
			x"00",x"01",x"02",x"03",x"04",x"05",x"06",x"07",x"08",x"09",x"0a",x"0b",x"0c",x"0d",x"0e",x"0f",
		  	x"10",x"11",x"12",x"13",x"14",x"15",x"16",x"17",x"18",x"19",x"1a",x"1b",x"1c",x"1d",x"1e",x"1f",
		  	x"20",x"21",x"22",x"23",x"24",x"25",x"26",x"27",x"28",x"29",x"2a",x"2b",x"2c",x"2d",x"2e",x"2f",
			x"30",x"31",x"32",x"33",x"34",x"35",x"36",x"37",x"38",x"39",x"3a",x"3b",x"3c",x"3d",x"3e",x"3f"
);


--Infer Block RAM for Xilinx FPGA                   
attribute ram_style : string;                       
attribute ram_style of memory : signal is "block";

-- Infer Block RAM for Lattice FPGA
attribute syn_ramstyle : string;
attribute syn_ramstyle of memory : signal is "block_ram";

begin

process(clk)
begin
	if rising_edge(clk) then
		if(we = '0') then
			memory(to_integer(unsigned(address))) <= datain;
		else
			dataout <= memory(to_integer(unsigned(address)));
		end if;
	end if;
end process;

end Behavioral;
