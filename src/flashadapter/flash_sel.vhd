----------------------------------------------------------------------------------
-- Company: STMicroelectronics
-- Engineer: David Siorpaes
-- 
-- Create Date: 03/09/2017 03:57:41 PM
-- Module Name: flash_sel - Behavioral
-- Description: Tweaks MOSI signal so to modify bits 11 and 12 after
--   Chip select. Used to map first 512kB to different flash regions.
--   e.g.: if using a 2MB flahs you can map up to 4 different banks
--   using 'sel' signal
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.NUMERIC_STD.ALL;

entity flash_sel is
    Port ( mosi_host : in STD_LOGIC;
           miso_host : out STD_LOGIC;
           clk_host : in STD_LOGIC;
           cs_host : in STD_LOGIC;
         
           mosi_flash : out STD_LOGIC;
           miso_flash : in STD_LOGIC;
           clk_flash : out STD_LOGIC;
           cs_flash : out STD_LOGIC;
           
           sel : in STD_LOGIC_VECTOR (1 downto 0));
end flash_sel;

architecture Behavioral of flash_sel is

signal counter : integer := 0;

begin
	process (clk_host, cs_host)
	begin
		--Hold counter to 0 as long as Chip Select is high
		if(cs_host = '1') then
			counter <= 0;

		--Count incoming bits
		elsif(falling_edge(clk_host)) then
				counter <= counter + 1;
		end if;
		
	end process;

	--Replicate all signals but MOSI as is 
	miso_host  <= miso_flash;
	clk_flash  <= clk_host;
	cs_flash   <= cs_host;

	--Tweak MOSI according to sel inputs
	mosi_flash <= sel(1)    when (counter = 11) else
				  sel(0)    when (counter = 12) else
				  mosi_host;
end Behavioral;
