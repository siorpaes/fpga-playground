----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/09/2017 05:47:26 PM
-- Design Name: 
-- Module Name: flash_sel_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity flash_sel_tb is
end flash_sel_tb;

architecture Behavioral of flash_sel_tb is
	-- SPI clock = 10MHz => 100 nanoseconds period
	constant c_SPI_CLOCK_PERIOD : time := 100 ns;
	
	-- Chip Select timing
	constant c_CS_HOLD : time := 1 us;
	
	-- Inter SPI data timing
	constant c_DATA_HOLD : time := 500 ns;
	
	--SPI clock, mosi, cs
	signal r_CLOCK_HOST     : std_logic := '0';
	signal r_MOSI_HOST      : std_logic := '0';
	signal r_CS_HOST        : std_logic := '0';
	signal r_MOSI_FLASH     : std_logic := '0';

	--Declare Unit Under Test
	component flash_sel is
		Port ( mosi_host : in STD_LOGIC;
			   miso_host : out STD_LOGIC;
			   clk_host : in STD_LOGIC;
			   cs_host : in STD_LOGIC;
			 
			   mosi_flash : out STD_LOGIC;
			   miso_flash : in STD_LOGIC;
			   clk_flash : out STD_LOGIC;
			   cs_flash : out STD_LOGIC;
			   
			   sel : in STD_LOGIC_VECTOR (1 downto 0));
	end component flash_sel;
begin

	-- Instantiate the Unit Under Test (UUT)
	UUT : flash_sel
		port map (
			clk_host   => r_CLOCK_HOST,
			mosi_host  => r_MOSI_HOST,
			cs_host    => r_CS_HOST,
			mosi_flash => r_MOSI_FLASH,
			miso_flash => '1',
			sel(0)     => '1',
			sel(1)     => '1'
		);
 
 	--Generate SPI Traffic
	process
	-- Emit bits over MOSI
	procedure emit_bit(value: in std_logic) is
		begin
			r_MOSI_HOST <= value;
			wait for c_SPI_CLOCK_PERIOD/2;
			r_CLOCK_HOST <= not r_CLOCK_HOST;
    		wait for c_SPI_CLOCK_PERIOD/2;
			r_CLOCK_HOST <= not r_CLOCK_HOST;
		end emit_bit;

	-- Emit bytes over MOSI, MSB first
	procedure emit_byte(value: std_logic_vector(7 downto 0)) is
		begin
			for i in value'length-1 downto 0 loop
				emit_bit(value(i));
			end loop;
		end emit_byte;

 		begin 		
		r_CLOCK_HOST <= '0';
		
 		--Drive CS low
 		r_CS_HOST <= '1';	
 		wait for c_CS_HOLD;
 		r_CS_HOST <= '0';
 		wait for c_CS_HOLD;

		-- Emit MOSI data that instructs flash memory read from 0x02000000
		-- Bits 11 and 12 should take the values of 'sel' signal
		emit_byte(x"03");
		wait for c_DATA_HOLD;
		emit_byte(x"02");
		wait for c_DATA_HOLD;
		emit_byte(x"00");
		wait for c_DATA_HOLD;
		emit_byte(x"00");
		wait for c_DATA_HOLD;

    	--Drive CS hi
    	wait for c_CS_HOLD;
    	r_CS_HOST <= '1';    	
		wait for c_CS_HOLD;
 	end process;
 	

end Behavioral;
