module RGB
(
    // outputs
    output  wire        REDn,
    output  wire        BLUn,
    output  wire        GRNn,
    input   wire        red,
    input   wire        green,
    input   wire        blue
);


//----------------------------------------------------------------------------
//                                                                          --
//                       Instantiate RGB primitive                          --
//                                                                          --
//----------------------------------------------------------------------------
    SB_RGBA_DRV RGB_DRIVER ( 
      .RGBLEDEN (1'b1),
      .RGB0PWM  (green),
      .RGB1PWM  (blue),
      .RGB2PWM  (red),
      .CURREN   (1'b1), 
      .RGB0     (GRNn),		//Actual Hardware connection
      .RGB1     (BLUn),
      .RGB2     (REDn)
    );
    defparam RGB_DRIVER.RGB0_CURRENT = "0b000001";
    defparam RGB_DRIVER.RGB1_CURRENT = "0b000001";
    defparam RGB_DRIVER.RGB2_CURRENT = "0b000001";

endmodule // RGB
