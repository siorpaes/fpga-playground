/* Instantiates 48MHz oscillator on iCE40UP5K
 */

module CLOCK_SRC                                
(                                                   
    output wire clock        
);                                                  

/* Clock divider can be set using CLHF_DIV: 1, 2, 4, 8 */
SB_HFOSC  #(.CLKHF_DIV("0b00")) u_SB_HFOSC(.CLKHFPU(1), .CLKHFEN(1), .CLKHF(clock));

endmodule
