----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:28:51 06/25/2015 
-- Design Name: 
-- Module Name:    pdmtpcm - Behavioral 
-- Description:    Converts from PDM to PCM using decimation filter
--                 Includes trivial volume control
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity pdmtpcm is
    Port ( pdm_data   : in  STD_LOGIC;
           clock      : in  STD_LOGIC;
           pdm_clk    : out  STD_LOGIC;
           pcm_data   : out  STD_LOGIC_VECTOR (15 downto 0);
           pcm_strobe : out  STD_LOGIC;
           volume     : in STD_LOGIC_VECTOR(2 downto 0));
end pdmtpcm;

architecture Behavioral of pdmtpcm is
constant ntaps: integer := 298;
type lut is array (0 to ntaps) of std_logic_vector(7 downto 0);
--Triangular filter
signal filter : lut := (x"01",x"02",x"03",x"04",x"05",x"06",x"07",x"08",x"09",x"0a",x"0b",x"0c",x"0d",x"0e",x"0f",x"10",x"11",x"12",x"13",x"14",x"15",x"16",x"17",x"18",x"19",x"1a",x"1b",x"1c",x"1d",x"1e",x"1f",x"20",x"21",x"22",x"23",x"24",x"25",x"26",x"27",x"28",x"29",x"2a",x"2b",x"2c",x"2d",x"2e",x"2f",x"30",x"31",x"32",x"33",x"34",x"35",x"36",x"37",x"38",x"39",x"3a",x"3b",x"3c",x"3d",x"3e",x"3f",x"40",x"41",x"42",x"43",x"44",x"45",x"46",x"47",x"48",x"49",x"4a",x"4b",x"4c",x"4d",x"4e",x"4f",x"50",x"51",x"52",x"53",x"54",x"55",x"56",x"57",x"58",x"59",x"5a",x"5b",x"5c",x"5d",x"5e",x"5f",x"60",x"61",x"62",x"63",x"64",x"65",x"66",x"67",x"68",x"69",x"6a",x"6b",x"6c",x"6d",x"6e",x"6f",x"70",x"71",x"72",x"73",x"74",x"75",x"76",x"77",x"78",x"79",x"7a",x"7b",x"7c",x"7d",x"7e",x"7f",x"80",x"81",x"82",x"83",x"84",x"85",x"86",x"87",x"88",x"89",x"8a",x"8b",x"8c",x"8d",x"8e",x"8f",x"90",x"91",x"92",x"93",x"94",x"95",x"96",x"95",x"94",x"93",x"92",x"91",x"90",x"8f",x"8e",x"8d",x"8c",x"8b",x"8a",x"89",x"88",x"87",x"86",x"85",x"84",x"83",x"82",x"81",x"80",x"7f",x"7e",x"7d",x"7c",x"7b",x"7a",x"79",x"78",x"77",x"76",x"75",x"74",x"73",x"72",x"71",x"70",x"6f",x"6e",x"6d",x"6c",x"6b",x"6a",x"69",x"68",x"67",x"66",x"65",x"64",x"63",x"62",x"61",x"60",x"5f",x"5e",x"5d",x"5c",x"5b",x"5a",x"59",x"58",x"57",x"56",x"55",x"54",x"53",x"52",x"51",x"50",x"4f",x"4e",x"4d",x"4c",x"4b",x"4a",x"49",x"48",x"47",x"46",x"45",x"44",x"43",x"42",x"41",x"40",x"3f",x"3e",x"3d",x"3c",x"3b",x"3a",x"39",x"38",x"37",x"36",x"35",x"34",x"33",x"32",x"31",x"30",x"2f",x"2e",x"2d",x"2c",x"2b",x"2a",x"29",x"28",x"27",x"26",x"25",x"24",x"23",x"22",x"21",x"20",x"1f",x"1e",x"1d",x"1c",x"1b",x"1a",x"19",x"18",x"17",x"16",x"15",x"14",x"13",x"12",x"11",x"10",x"0f",x"0e",x"0d",x"0c",x"0b",x"0a",x"09",x"08",x"07",x"06",x"05",x"04",x"03",x"02",x"01");
signal signaltaps : std_logic_vector (ntaps downto 0);
begin
	--Emit PDM Clock
	pdm_clk <= clock;
		
	process(clock)
	variable N : integer range 0 to 200 := 0;
	variable pcm : unsigned (15 downto 0) := (others => '0');
	--Sample PDM: If L/R=Vcc sample on falling edge. If L/R=GND sample on rising edge
	begin
		if falling_edge(clock) then
			--Shift all previous stored bits
			for i in 0 to signaltaps'length-2 loop
				signaltaps(i) <= signaltaps(i+1);
			end loop;
			--Store incoming PDM data
			signaltaps(signaltaps'length-1) <= pdm_data;
			
			--Sum up. Note that it is mandatory to use a variable and not a signal otherwise
			--The pcm would just increment at each process activation. See page 807
			pcm := (others => '0');
			for i in 0 to signaltaps'length-1 loop
				if signaltaps(i) = '1' then
					pcm := pcm + unsigned(filter(i));
				end if;
			end loop;
			
			--Adjust volume shifting pcm to left
			pcm := shift_left(pcm, to_integer(unsigned(volume)));
			--Emit PCM data on 'parallel' port
			pcm_data <= std_logic_vector(pcm);
		end if;

		--Emit PCM strobe every N samples, where N is the decimation factor
		if rising_edge(clock) then
			if N=127 then
				pcm_strobe <= '1';
				N := 0;
			else
				pcm_strobe <= '0';
				N := N + 1;
			end if;
		end if;
	end process;
end Behavioral;

