----------------------------------------------------------------------------------
-- Company: STMicroelectronics
-- Engineer: David Siorpaes
-- 
-- Create Date: 09/27/2016 12:05:18 PM
-- Design Name: datagen
-- Module Name: datagen - Behavioral
-- Description: Generates 16-bit parallel data stream
--              Data changes on clock falling edge
--              SW0-SW7 set clock divider
--              Button C sets data output to known value (0xEA)
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity datagen is
    Port ( clk : in STD_LOGIC;
           divider : in STD_LOGIC_VECTOR (7 downto 0);
           btnC : in STD_LOGIC;
           dclk : out STD_LOGIC;
           data0 : out STD_LOGIC_VECTOR (7 downto 0);
           data1 : out STD_LOGIC_VECTOR (7 downto 0));
end datagen;

architecture Behavioral of datagen is
signal current_data : unsigned (8 downto 0) := (others => '0');
signal clk_cnt : unsigned (15 downto 0) := (others => '0');
signal tmp: STD_LOGIC;
begin
process(clk)
begin
	if(rising_edge(clk)) then
		if clk_cnt = unsigned(divider) then
			clk_cnt <= (others => '0');
			tmp <= not tmp;
			current_data <= current_data + 1;
		else
			clk_cnt <= clk_cnt + 1;
		end if;
	end if;
end process;

--Output clock and data
dclk <= tmp;
data0 <= std_logic_vector(current_data(8 downto 1)) when (btnC = '0') else "11101010";
data1 <= not std_logic_vector(current_data(8 downto 1)) when (btnC = '0') else "11101010";

end Behavioral;
