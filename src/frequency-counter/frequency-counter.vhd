----------------------------------------------------------------------------------
--
-- Create Date: 09/18/2017 12:27:46 PM
-- Design Name: Frequency Counter
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- CLK_FREQ:     Input clock frequency
-- CLK_DIVISOR:  Clock Divisor for SPI-like output
entity frequency_counter is
    generic ( CLK_FREQ:      POSITIVE := 100_000_000;
              CLK_DIVISOR :  POSITIVE := 100);
    Port ( clk        : in  STD_LOGIC;   -- Input clock
           input      : in  STD_LOGIC;   -- Input signal
           spi_output : out STD_LOGIC;   -- Serialized value of measured frequency
           clk_out    : out STD_LOGIC;   -- Clock for reading frequency value
           freq_value : out STD_LOGIC_VECTOR (15 downto 0)); -- 16 least significant bits of frequency value
end frequency_counter;

architecture Behavioral of frequency_counter is

-- Clock counter
signal clk_cnt     : unsigned (31 downto 0) := (others => '0');

-- Signal counters
signal s_count        : unsigned (31 downto 0) := (others => '0');
signal s_count_khz    : unsigned (31 downto 0) := (others => '0');
signal freq_value_int : unsigned (31 downto 0) := (others => '0');

-- Clock divider
signal clk_div_cnt      : unsigned (31 downto 0) := (others => '0');
signal clk_div          : std_logic := '0';

-- SPI transmission signal
signal spi_cnt          : unsigned (6 downto 0) := (others => '0');

begin

-- Counts clock edges and stores current value of signal rising edges (s_count) when wrapping at 1 second
process(clk)
begin
	if(rising_edge(clk)) then
		if clk_cnt = (CLK_FREQ-1) then
			clk_cnt <= (others => '0');
			freq_value_int <= s_count;
		else
			clk_cnt <= clk_cnt + 1;
		end if;
	end if;
end process;

--Emit 16LSB of frequency value
freq_value <= std_logic_vector(freq_value_int(15 downto 0));

-- Counts input signal edges
-- Reset counter when wrapping at 1 second
process(input, clk_cnt)
begin
    if(clk_cnt = 0) then
		s_count <= (others => '0');
    else
        if(rising_edge(input)) then
            s_count <= s_count + 1;
        end if;
    end if;
end process;


--- Clock divider for SPI transmission
process(clk)
begin
	if(rising_edge(clk)) then
		if clk_div_cnt = (CLK_DIVISOR-1) then
		    clk_div_cnt <= (others => '0');
		    clk_div <= not clk_div;			
		else
			clk_div_cnt <= clk_div_cnt + 1;
		end if;
	end if;
end process;


-- Transmit current data every now and then (see length of spi_cnt signal) via SPI-like port
process (clk_div)
begin
    if(rising_edge(clk_div)) then
        if spi_cnt < 32 then
            spi_output <= freq_value_int(to_integer(spi_cnt));
        else
            spi_output <= '0';
        end if;

        spi_cnt <= spi_cnt + 1;
    end if;
end process;

--Emit clock for SPI. Note that we have to delay emission by one as spi_cnt is updated in above process
clk_out <= clk_div when (spi_cnt - 1) < 32 else '0';

end Behavioral;
