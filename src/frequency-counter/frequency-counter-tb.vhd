----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/19/2017 11:24:31 AM
-- Design Name: 
-- Module Name: frequency-counter-tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity frequency_counter_tb is
end frequency_counter_tb;

architecture Behavioral of frequency_counter_tb is

-- Clock 100MHZ
constant c_CLOCK_PERIOD : time := 10 ns;

-- Signal 1kHz
constant c_SIGNAL_PERIOD : time := 1 ms;


-- Testbench signals
signal r_clk        : STD_LOGIC := '0';
signal r_input      : STD_LOGIC := '0';
signal r_spi_output : STD_LOGIC := '0';
signal r_clk_out    : STD_LOGIC := '0';
signal r_freq_value : STD_LOGIC_VECTOR (15 downto 0);


-- Declare component
component frequency_counter is
    Port ( clk        : in  STD_LOGIC;
           input      : in  STD_LOGIC;
           spi_output : out STD_LOGIC;
           clk_out    : out STD_LOGIC;
           freq_value : out STD_LOGIC_VECTOR (15 downto 0));
end component frequency_counter;

begin

-- Instantiate UUT
UUT: frequency_counter
    port map(
    clk         => r_clk,
    input       => r_input,
    spi_output  => r_spi_output,
    clk_out     => r_clk_out,
    freq_value  => r_freq_value
);


-- Generate Clock
p_CLK_GEN : process is
  begin
    wait for c_CLOCK_PERIOD/2;
    r_clk <= not r_clk;
end process p_CLK_GEN;


-- Generate input signal
p_SIG_GEN : process is
  begin
    wait for c_SIGNAL_PERIOD/2;
    r_input <= not r_input;
end process p_SIG_GEN;


end Behavioral;
