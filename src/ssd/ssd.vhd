-- Seven Segment Display driver

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity ssd is
    -- CLK_DIVISOR:  Clock Divisor for SSD digits mux. Tune to provide about 1kHz
    generic (CLK_DIVISOR :  POSITIVE := 100000);
    Port ( clk     : in  STD_LOGIC;   -- Input clock
           reset   : in  STD_LOGIC;   -- Reset
           digit0  : in  STD_LOGIC_VECTOR (3 downto 0); -- First digit value
           digit1  : in  STD_LOGIC_VECTOR (3 downto 0); -- Second digit value
           digit2  : in  STD_LOGIC_VECTOR (3 downto 0); -- Third digit value
           digit3  : in  STD_LOGIC_VECTOR (3 downto 0); -- Fourth digit value
           seg     : out STD_LOGIC_VECTOR (6 downto 0); -- Segments
           an      : out STD_LOGIC_VECTOR (3 downto 0)  -- Display anodes
          );
end ssd;

architecture Behavioral of ssd is

-- Clock divider
signal clk_div_cnt    : unsigned (31 downto 0) := (others => '0');
signal clk_div        : std_logic := '0';

-- SSD index. Used to select currently decoded display
signal ssd_idx        : unsigned (1 downto 0) := (others => '0');

-- Hold current decoded digit
signal digit          : STD_LOGIC_VECTOR (3 downto 0);


begin
--- Clock divider for SSD mux
process(clk)
begin
	if(rising_edge(clk)) then
		if clk_div_cnt = (CLK_DIVISOR-1) then
			clk_div_cnt <= (others => '0');
			clk_div <= not clk_div;
		else
			clk_div_cnt <= clk_div_cnt + 1;
		end if;
	end if;
end process;


-- Update currently selected SSD display
process (clk_div)
begin
	if(rising_edge(clk_div)) then
		ssd_idx <= ssd_idx + 1;
	end if;
end process;

-- Demux digit 
with ssd_idx select
	digit <= digit0 when "00",
	         digit1 when "01",
	         digit2 when "10",
	         digit3 when "11";

-- Anode driving
with ssd_idx select
	an <= "1110" when "00",
	      "1101" when "01",
	      "1011" when "10",
	      "0111" when "11";

--SSD decoding
process (ssd_idx, digit)
begin
case digit is
	when "0000" => seg <="0000001";  -- '0'                              
	when "0001" => seg <="1001111";  -- '1'                              
	when "0010" => seg <="0010010";  -- '2'                              
	when "0011" => seg <="0000110";  -- '3'                              
	when "0100" => seg <="1001100";  -- '4'                              
	when "0101" => seg <="0100100";  -- '5'                              
	when "0110" => seg <="0100000";  -- '6'                              
	when "0111" => seg <="0001111";  -- '7'                              
	when "1000" => seg <="0000000";  -- '8'                              
	when "1001" => seg <="0000100";  -- '9'
	when "1010" => seg <="0001000";  -- 'A'
	when "1011" => seg <="1100000";  -- 'b'
	when "1100" => seg <="0110001";  -- 'C'
	when "1101" => seg <="1000010";  -- 'd'
	when "1110" => seg <="0110000";  -- 'E'
	when "1111" => seg <="0111000";  -- 'F'
	when others => seg <="1111111";
end case;
end process;

end Behavioral;
